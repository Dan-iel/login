<?php
require 'connexion.php';
require 'header.php';
session_start();
?>

    <main>
        <?php
        if(isset($_SESSION["test"])){
            include 'connected.php';
        } else {
            include 'login.php';
        }
        ?>
    </main>


    <?php
    require 'footer.php'
    ?>
